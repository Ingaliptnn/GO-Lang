package main

import (
	_ "fmt"
	"html/template"
	"net/http"
	_ "strconv"
)

type Car struct {
	Brand       string
	Model       string
	Image       string
	Description string
}

type Cars struct {
	Cars []Car
}

func render(w http.ResponseWriter, page string, data interface{}) { //функция для вывода странички

	tmpl := make(map[string]*template.Template)
	tmpl[page] = template.Must(template.ParseFiles("static/pages/"+page+".html", "template.html"))
	tmpl[page].ExecuteTemplate(w, "base", data)
}
func actionIndex(w http.ResponseWriter, r *http.Request) {
	main_slider := Cars{
		Cars: []Car{
			{Image: "es.jpg"},
			{Image: "rx.jpeg"},
			{Image: "ls.jpg"},
			{Image: "nx.png"},
		},
	}
	render(w, "index", main_slider)
}
func actionCars(w http.ResponseWriter, r *http.Request) {
	data := getCarsArray()
	render(w, "cars", data)
}
func getCarsArray() Cars {
	return Cars{
		Cars: []Car{
			{Brand: "Lexus", Model: "ES", Image: "es.jpg", Description: "Luxury meets performance in the Lexus ES. With its refined design and advanced features, the ES offers a smooth and comfortable driving experience."},
			{Brand: "Lexus", Model: "RX", Image: "rx.jpeg", Description: "Experience the epitome of SUV luxury with the Lexus RX. Its spacious interior, cutting-edge technology, and powerful performance redefine the driving experience."},
			{Brand: "Lexus", Model: "LS", Image: "ls.jpg", Description: "Elevate your driving experience with the Lexus LS. This flagship sedan combines exquisite craftsmanship, advanced technology, and unmatched comfort for a truly luxurious ride."},
			{Brand: "Lexus", Model: "NX", Image: "nx.png", Description: "The Lexus NX seamlessly blends style and versatility. With its compact design and innovative features, the NX delivers a dynamic driving experience without compromising on luxury."},
		},
	}
}

func actionCarInfo(w http.ResponseWriter, r *http.Request) {
	var data = Car{
		Brand:       "",
		Model:       "На жаль інформацію про даний автомобіль не знайдено",
		Image:       "no_image.svg",
		Description: "Не знайдено",
	}
	if r.Method == "GET" {
		carName := r.FormValue("name")
		cars := getCarsArray().Cars
		for i := 0; i < len(cars); i++ {
			if cars[i].Brand+"_"+cars[i].Model == carName {
				data = cars[i]
			}
		}
	}
	render(w, "car-info", data)
}
func actionService(w http.ResponseWriter, r *http.Request) {
	render(w, "service", "")
}
func actionContacts(w http.ResponseWriter, r *http.Request) {
	if r.Method == "POST" { // Обрабатываем входные данные
		err := r.ParseForm() // Парсим форму
		post := r.PostForm
		if err != nil {

			return
		}
		user_name := post.Get("user_name")
		email := post.Get("email")
		decription := post.Get("description")

		type ContactAnswer struct {
			Name        string
			Email       string
			Description string
		}
		info := ContactAnswer{
			Name:        user_name,
			Email:       email,
			Description: decription,
		}
		render(w, "contact-feedback", info)
	} else {

		render(w, "contact", "")
	}

}

func staticRoutes() { //Обработчики для статичных файлов
	http.Handle("/media/", http.StripPrefix("/media/", http.FileServer(http.Dir("media"))))
	http.Handle("/css/", http.StripPrefix("/css/", http.FileServer(http.Dir("css"))))
	http.Handle("/js/", http.StripPrefix("/js/", http.FileServer(http.Dir("js"))))
}
func pagesRoutes() { //Обработчики для страниц
	http.HandleFunc("/", actionIndex)
	http.HandleFunc("/cars", actionCars)
	http.HandleFunc("/car-info", actionCarInfo)
	http.HandleFunc("/service", actionService)
	http.HandleFunc("/contact", actionContacts)

}

func main() {
	// Запускаем локальный сервер
	staticRoutes()
	pagesRoutes()
	http.ListenAndServe("localhost:80", nil)
}
