package main

import (
	"fmt"
)

func main() {
	fmt.Println("Синоніми цілих типів\n")

	fmt.Println("byte    - int8")
	fmt.Println("rune    - int32")
	fmt.Println("int     - int32, чи int64, залежно від ОС")
	fmt.Println("uint    - uint32, или uint64, залежно від ОС")

	// Завдання.
	//1. Визначити розрядність ОС
	const is64Bit = uint64(^uintptr(0)) == ^uint64(0)
	if is64Bit {
		fmt.Println("ОС - 64 біт")
	} else {
		fmt.Println("ОС - 32 біт")
	}
}
